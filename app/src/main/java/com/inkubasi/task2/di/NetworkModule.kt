package com.inkubasi.task2.di

import com.inkubasi.task2.BuildConfig
import com.inkubasi.task2.data.remote.network.ApiHelper
import com.inkubasi.task2.data.remote.network.ApiHelperImpl
import com.inkubasi.task2.data.remote.network.ApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private fun providesHttpLogginInterceptor() = HttpLoggingInterceptor()
    .apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

private fun providesOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
    OkHttpClient.Builder()
        .addInterceptor(httpLoggingInterceptor)
        .build()

private fun providesRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
    .baseUrl(BuildConfig.BASE_URL)
    .client(okHttpClient)
    .addConverterFactory(GsonConverterFactory.create())
    .build()

private fun providesApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

val networkModule = module {
    single { providesHttpLogginInterceptor() }
    single { providesOkHttpClient(get()) }
    single { providesRetrofit(get()) }
    single { providesApiService(get()) }
    single <ApiHelper> {
        ApiHelperImpl(get())
    }
}