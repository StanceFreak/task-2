package com.inkubasi.task2.di

import com.inkubasi.task2.data.remote.repository.NewsRepository
import org.koin.dsl.module

val repoModule = module {
    single { NewsRepository(get()) }
}