package com.inkubasi.task2

import android.app.Application
import com.inkubasi.task2.di.networkModule
import com.inkubasi.task2.di.repoModule
import com.inkubasi.task2.di.viewModelModule
import org.koin.core.context.startKoin

class BaseApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(networkModule, viewModelModule, repoModule)
        }
    }

}