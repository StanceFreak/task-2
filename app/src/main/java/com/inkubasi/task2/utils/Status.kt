package com.inkubasi.task2.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}