package com.inkubasi.task2.views.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.inkubasi.task2.databinding.FragmentHomeBinding
import com.inkubasi.task2.utils.Status
import com.inkubasi.task2.views.adapter.HomeNewsAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var newsAdapter: HomeNewsAdapter
    private val viewModel: HomeViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
        setupViews()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun setupAdapter() {
        newsAdapter = HomeNewsAdapter()
        binding.rvHome.apply {
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = newsAdapter
        }
    }

    private fun setupViews() {
        viewModel.getGameNews().observe(viewLifecycleOwner) {
            it.let { resource ->
                binding.apply {
                    when (resource.status) {
                        Status.SUCCESS -> {
                            rvHome.visibility = View.VISIBLE
                            shimmerHomeContainer.visibility = View.GONE
                            resource.data?.let { response ->
                                response.body()?.let { data -> newsAdapter.setData(data) }
                            }
                        }
                        Status.ERROR -> {
                            shimmerHomeContainer.stopShimmer()
                            shimmerHomeContainer.visibility = View.INVISIBLE
                            Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                        }
                        Status.LOADING -> {
                            shimmerHomeContainer.visibility = View.VISIBLE
                            rvHome.visibility = View.GONE
                        }
                    }
                }
            }
        }
    }
}