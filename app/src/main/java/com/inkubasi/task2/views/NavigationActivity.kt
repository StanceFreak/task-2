package com.inkubasi.task2.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.inkubasi.task2.R
import com.inkubasi.task2.databinding.ActivityNavigationBinding
import com.inkubasi.task2.views.home.HomeFragment

class NavigationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNavigationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNavigationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupNav()
    }

    private fun setupNav() {
        val navHost = supportFragmentManager.findFragmentById(R.id.fragment_navi_container) as NavHostFragment
        val navController = navHost.navController
        val appBarConfig = AppBarConfiguration(
            setOf(
                R.id.homeFragment,
                R.id.favoriteFragment,
                R.id.profileFragment
            )
        )
        binding.botnavNaviContainer.apply {
            setupWithNavController(navController)
            setOnItemSelectedListener {
                when (it.itemId) {
                    R.id.botnav_home -> {
                        loadFragment(HomeFragment())
                        true
                    }
                    R.id.botnav_favorite -> {
                        loadFragment(FavoriteFragment())
                        true
                    }
                    R.id.botnav_profile -> {
                        loadFragment(ProfileFragment())
                        true
                    }
                    else -> {
                        loadFragment(HomeFragment())
                        true
                    }
                }
            }
        }
        navController.addOnDestinationChangedListener { _: NavController, destination: NavDestination, _: Bundle? ->
            binding.botnavNaviContainer.isVisible = appBarConfig.topLevelDestinations.contains(destination.id)
        }
    }

    private fun loadFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_navi_container, fragment)
            .addToBackStack(fragment::class.java.simpleName)
            .commit()
    }
}