package com.inkubasi.task2.views.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.inkubasi.task2.data.remote.repository.NewsRepository
import com.inkubasi.task2.utils.Resource
import kotlinx.coroutines.Dispatchers

class HomeViewModel(
    private val newsRepository: NewsRepository
): ViewModel() {

    fun getGameNews() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = newsRepository.getGameNews()))
        }
        catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occured!"))
        }
    }

}