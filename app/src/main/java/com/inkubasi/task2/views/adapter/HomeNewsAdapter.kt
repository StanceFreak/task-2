package com.inkubasi.task2.views.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.inkubasi.task2.R
import com.inkubasi.task2.data.remote.model.NewsResponseItem
import com.inkubasi.task2.databinding.RvHomeBinding
import com.squareup.picasso.Picasso
import java.lang.Integer.min

class HomeNewsAdapter: RecyclerView.Adapter<HomeNewsAdapter.RecyclerViewHolder>() {

    private var data = ArrayList<NewsResponseItem>()

    inner class RecyclerViewHolder (private val binding: RvHomeBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: NewsResponseItem) {
            binding.apply {
                Picasso.get()
                    .load(item.thumb)
                    .placeholder(R.drawable.ic_image_placeholder)
//                    .fit()
                    .into(ivRvItemThumbnail)
                tvRvItemTitle.text = item.title
                tvRvItemAuthor.text = item.author
                tvRvItemDate.text = item.time
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val itemBinding = RvHomeBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return RecyclerViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val data = this.data[position]
        holder.bind(data)
    }

    override fun getItemCount(): Int {
        return min(this.data.size, 10)
    }

    fun setData(dataList: List<NewsResponseItem>) {
        this.data.clear()
        this.data.addAll(dataList)
        notifyDataSetChanged()
    }
}