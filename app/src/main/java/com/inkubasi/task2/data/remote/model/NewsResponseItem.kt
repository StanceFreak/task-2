package com.inkubasi.task2.data.remote.model


import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import android.os.Parcelable

@Parcelize
data class NewsResponseItem(
    @SerializedName("title")
    val title: String,
    @SerializedName("thumb")
    val thumb: String,
    @SerializedName("author")
    val author: String,
    @SerializedName("tag")
    val tag: String,
    @SerializedName("time")
    val time: String,
    @SerializedName("desc")
    val desc: String,
    @SerializedName("key")
    val key: String
) : Parcelable