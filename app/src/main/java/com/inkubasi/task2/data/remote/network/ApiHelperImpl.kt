package com.inkubasi.task2.data.remote.network

import com.inkubasi.task2.data.remote.model.NewsResponse
import retrofit2.Response

class ApiHelperImpl(
    private val apiService: ApiService
): ApiHelper{

    override suspend fun getGameNews(): Response<NewsResponse> {
        return apiService.getGameNews()
    }

}