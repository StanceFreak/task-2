package com.inkubasi.task2.data.remote.network

import com.inkubasi.task2.data.remote.model.NewsResponse
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("games/news?page=1")
    suspend fun getGameNews(): Response<NewsResponse>

}