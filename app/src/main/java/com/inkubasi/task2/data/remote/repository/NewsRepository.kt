package com.inkubasi.task2.data.remote.repository

import com.inkubasi.task2.data.remote.model.NewsResponse
import com.inkubasi.task2.data.remote.network.ApiHelper
import retrofit2.Response

class NewsRepository(
    private val apiHelper: ApiHelper
) {

    suspend fun getGameNews() : Response<NewsResponse> {
        return apiHelper.getGameNews()
    }

}