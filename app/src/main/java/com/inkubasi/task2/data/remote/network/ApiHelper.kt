package com.inkubasi.task2.data.remote.network

import com.inkubasi.task2.data.remote.model.NewsResponse
import retrofit2.Response

interface ApiHelper {
    suspend fun getGameNews() : Response<NewsResponse>
}